# Read a list of integers:
N, k = [int(s) for s in input().split()]
pinos = ["I"] * N
lista_pinos = []
for i in range(k):
    a, b = [int(s) for s in input().split()]
    lista_pinos.append((a, b))

for c, p in lista_pinos:
    for num_pinos in range(c - 1, p): 
        pinos[num_pinos] = '.' 
#print(pins)
print(''.join(pinos))