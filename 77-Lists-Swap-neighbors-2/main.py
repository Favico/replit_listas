# Read a list of integers:
x = input()
lista = list(map(int, x.split()))

for i in range(0, len(lista), 2):
  popval = lista.pop(i)
  lista.insert(i+1, popval)
  
print(lista)