# Read a list of integers:
a = [int(s) for s in input().split()]
for num in range(1, len(a)):
    if a[num] > a[num-1]:
        print(a[num], end=' ')